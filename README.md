# Team Cyberpunk 1977's submission to the AI module
Pierre Pinet (pierre.pinet@epitech.eu)

Ethan Margaillan (ethan.margaillan@epitech.eu)

Guillaume Bergignat (guillaume.bergignat@epitech.eu)

This is a fork of https://github.com/nlehir/phantom_opera. Only the files inspector.py and fantom.py have been added/modified. They are weight-based AIs.

## Description of the game

You can find the rules of the game in ./le-fantom-de-l-opera_rules_fr.pdf

## To launch a game

1) python3.6 server.py

2) python3.6 inspector.py

3) python3.6 fantom.py

You can also use a more recent version of python.
