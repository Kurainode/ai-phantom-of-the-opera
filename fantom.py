import json
import logging
import os
import random
import socket
import math
from logging.handlers import RotatingFileHandler

import protocol

host = "localhost"
port = 12000
# HEADERSIZE = 10

"""
set up fantom logging
"""
fantom_logger = logging.getLogger()
fantom_logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s :: %(levelname)s :: %(message)s", "%H:%M:%S")
# file
if os.path.exists("./logs/fantom.log"):
    os.remove("./logs/fantom.log")
file_handler = RotatingFileHandler('./logs/fantom.log', 'a', 1000000, 1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
fantom_logger.addHandler(file_handler)
# stream
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.WARNING)
fantom_logger.addHandler(stream_handler)


class Player():

    # Weight reflecting the relative importance of a character's power
    # Red is the most important character, as his ability can eliminate a suspect instantaneously
    character_importance = {
        'red': 8,
        'grey': 6,
        'black': 3,
        'white': 3,
        'blue': 2,
        'brown': 1,
        'pink': 0,
        'purple': 0
    }

    current_character = {}

    def __init__(self):

        self.end = False
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    def connect(self):
        self.socket.connect((host, port))

    def reset(self):
        self.socket.close()

    def get_room_info(self, game_state):
        characters = game_state["characters"]
        rooms = [[]] * 10
        for character in characters:
            rooms[character['position']].append(character['suspect'])
        return rooms

    # We want to move the same characters as the inspector
    def move_character(self, data, game_state):
        max_value = -math.inf
        selected = 0
        suspect_weight = sum(1 for _ in filter(lambda x: not x['suspect'], game_state['characters']))
        for i in range(len(data)):
            current_value = 0
            current_value += self.character_importance[data[i]['color']]
            # If the character is suspect, we should try to move him to a populated room
            if data[i]['suspect']:
                current_value += suspect_weight + 2
            room_mates = filter(lambda x: x['position'] == data[i]['position'], game_state['characters'])
            if game_state['shadow'] == data[i]['position']:
                current_value += (sum(1 for _ in room_mates) - 1)
            else:
                current_value -= (sum(1 for _ in room_mates) - 1)
            if current_value > max_value:
                max_value = current_value
                selected = i
        self.current_character = data[selected]
        return selected

    def select_position(self, data, game_state):
        max_value = -math.inf
        selected = 0
        room_info = self.get_room_info(game_state)
        for i in range(len(data)):
            current_value = 0
            room_mates = filter(lambda x: x['position'] == data[i], game_state['characters'])
            room_mates_count = (sum(1 for _ in room_mates) - 1)
            suspect_mates = filter(lambda x: x['suspect'], room_mates)
            suspect_mates_count = (sum(1 for _ in suspect_mates) - self.current_character["suspect"])
            current_value += suspect_mates_count * 2
            if game_state['shadow'] == data[i] and self.current_character['suspect'] is False:
                if sum(room_info[data[i]]) <= 1:
                    current_value -= 5
                else:
                    current_value += (sum(1 for _ in room_mates) - 1)
            elif game_state['shadow'] == data[i]:
                current_value += 5
            if current_value > max_value:
                max_value = current_value
                selected = i
        return selected

    def activate_power(self, color, game_state):
        room_mates = filter(lambda x: x['position'] == self.current_character['position'], game_state['characters'])
        room_mates_count = (sum(1 for _ in room_mates) - 1)
        suspect_mates = filter(lambda x: x['suspect'], room_mates)
        suspect_mates_count = (sum(1 for _ in suspect_mates) - self.current_character["suspect"])
        if color is "brown":
            if 0 < suspect_mates_count != room_mates_count:
                return 0
            elif 0 <= suspect_mates_count == room_mates_count:
                return 1
            else:
                return 0
        elif color is "black":
            return self.current_character["suspect"]
        elif color is "white":
            return False if suspect_mates_count > 0 else True
        else:
            room_info = self.get_room_info(game_state)
            if self.current_character['suspect']:
                return sum(room_info[self.current_character['position']]) > 2
        return 0

    def white_power(self, color, data, game_state):
        room_info = self.get_room_info(game_state)
        best = -1
        for i in range(len(data)):
            if sum(room_info[data[i]]) == 0:
                return i
            if sum(room_info[data[i]]) > 1:
                best = i
            elif best == -1:
                best = i
        return best if best > -1 else 0

    def purple_power(self, data, game_state):
        character = next(filter(lambda x: x['suspect'] == self.current_character['suspect'] and x['color'] != 'purple', game_state['characters']))
        return data.index(character['color'])

    def brown_power(self, data, game_state):
        return random.randint(0, len(data) - 1)

    def grey_power(self, data, game_state):
        room_info = self.get_room_info(game_state)
        best = -1
        for i in range(len(data)):
            if sum(room_info[data[i]]) > 1:
                return i
            elif sum(room_info[data[i]]) == 0:
                best = i
        return best if best > -1 else 0

    def blue_power_room(self, data, game_state):
        return random.randint(0, len(data) - 1)

    def blue_power_exit(self, data, game_state):
        return random.randint(0, len(data) - 1)

    def answer(self, question):
        # work
        question_type = question['question type']
        data = question["data"]
        game_state = question["game state"]
        # print(json.dumps(game_state, indent=2))

        # Move decisions
        if question_type == 'select character':
            response_index = self.move_character(data, game_state)
        elif question_type == 'select position':
            response_index = self.select_position(data, game_state)

        # Power decisions
        elif 'activate ' in question_type:
            response_index = self.activate_power(question_type.split()[1], game_state)
        elif 'white character power move' in question_type:
            response_index = self.white_power(question_type.split()[-1], data, game_state)
        elif question_type == 'purple character power':
            response_index = self.purple_power(data, game_state)
        elif question_type == 'brown character power':
            response_index = self.brown_power(data, game_state)
        elif question_type == 'grey character power':
            response_index = self.grey_power(data, game_state)
        elif question_type == 'blue character power room':
            response_index = self.blue_power_room(data, game_state)
        elif question_type == 'blue character power exit':
            response_index = self.blue_power_exit(data, game_state)
        else:
            response_index = random.randint(0, len(data) - 1)

        # log
        fantom_logger.debug("|\n|")
        fantom_logger.debug("fantom answers")
        fantom_logger.debug(f"question type ----- {question['question type']}")
        fantom_logger.debug(f"data -------------- {data}")
        fantom_logger.debug(f"response index ---- {response_index}")
        fantom_logger.debug(f"response ---------- {data[response_index]}")
        return response_index

    def handle_json(self, data):
        data = json.loads(data)
        response = self.answer(data)
        # send back to server
        bytes_data = json.dumps(response).encode("utf-8")
        protocol.send_json(self.socket, bytes_data)

    def run(self):

        self.connect()

        while self.end is not True:
            received_message = protocol.receive_json(self.socket)
            if received_message:
                self.handle_json(received_message)
            else:
                print("no message, finished learning")
                self.end = True


p = Player()

p.run()
